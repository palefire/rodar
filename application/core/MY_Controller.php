<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Controller extends MX_Controller {

    public $data;

    function __construct() {
        parent:: __construct();
        date_default_timezone_set("Asia/Kolkata");
        // Front End
        $this->data['css_path'] = base_url('/assets/css/');
        $this->data['js_path'] = base_url('/assets/js/');
        $this->data['img_path'] = base_url('/assets/images/');
       
    }//constructor

    
    public function sendsms( $ph_no=NULL, $msg_content=NULL ){

        $url = 'https://api.msg91.com/api/v5/otp?extra_param={"Param1":"Value1", "Param2":"Value2", "Param3": "Value3"}&authkey=347811AIc0HBvTTgH95fbcb21bP1&template_id=5fbcb7e084311351f503609a&mobile=91'.$ph_no.'&invisible=1&otp='.$msg_content.'&userip=IPV4 User IP&email=Email ID';
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          //https://api.msg91.com/api/v5/otp?extra_param={"Param1":"Value1", "Param2":"Value2", "Param3": "Value3"}&authkey=347811AIc0HBvTTgH95fbcb21bP1&template_id=5fbcb7e084311351f503609a&mobile=8961819473&invisible=1&otp=1234&userip=IPV4 User IP&email=Email ID
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTPHEADER => array(
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }


    }//function
}//class