 <section class="first-section" style="background-image: url(https://images.pexels.com/photos/3805096/pexels-photo-3805096.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);">
    <div class="content">
      <h2 class="content-title">Get in the driver's seat and get paid</h2>
      <p class="block">Drive on the platform with the largest network of active riders</p>
      <a href="#" class="signbtn">Signup up to drive</a>
      <p class="block2"><a href="#" class="overline-from-left">Learn more about driving and delivering</a></p>
    </div>
  </section>
  <section class="second-section" style="background-image: url(<?php echo site_url('assets/images/section2.jpg') ?>);">
    <div class="content">
      <h3 class="content-title">Setting 10,000+ cities in motion</h3>
      <p class="block"><a href="" class="overline-from-left">View all cities</a></p>
    </div>
  </section>
  <section class="third-section">
    <h4 class="title">Uber for Business</h4>
    <p class="block">The power of Uber in everyday business</p>
    <div class="row">
      <div class="col-md-4">
        <div class="content">
          <div class="item">
            <img src="https://www.uber-assets.com/image/upload/f_auto,q_auto:eco,c_fill,w_360,h_240/v1585954525/assets/14/fcb55f-8d2c-4037-be40-96265930413e/original/business-eater-horz2x.png">
          </div>
          <div class="topic">
            <h6 class="topic-title">Work meals</h6>
            <p class="topic-block">Delight your employees with their favorite meals delivered directly to anywhere they work.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
          <div class="item">
            <img src="https://www.uber-assets.com/image/upload/f_auto,q_auto:eco,c_fill,w_360,h_240/v1585954625/assets/6a/4717e8-ac27-493e-a940-990e87d31f02/original/employee-travel-horz2x.png">
          </div>
          <div class="topic">
            <h6 class="topic-title">Work meals</h6>
            <p class="block">Take the work out of work-related trips. Create ground travel and commute programs for employees.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
          <div class="item">
            <img src="https://www.uber-assets.com/image/upload/f_auto,q_auto:eco,c_fill,w_360,h_240/v1585954718/assets/e1/70f6a1-f828-414c-b3f1-fa39a6915711/original/ride-coordinator-horz2x.png">
          </div>
          <div class="topic">
            <h6 class="topic-title">Work meals</h6>
            <p class="block">Aim to impress. Arrange door-to-door rides for your customers, clients, and guests.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="fourth-section">
    <div class="row">
      <div class="col-md-6">
        <div class="content">
          <h2 class="fourth-title">Our commitment to your safety</h2>
          <p class="block">With every safety feature we add and every standard in our Community Guidelines we uphold, we're committed to working to create a safe environment for our users.</p>
          <p class="block2"><a href="#" class="overline-from-left">Learn more about our Community Guidelines</a></p>
          <p class="block3"><a href="#" class="overline-from-left">See all safety features</a></p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
          <img src="https://www.uber-assets.com/image/upload/q_auto:eco,c_fill,w_558,h_372/v1558736931/assets/e5/fb1f43-f1bf-4dd2-b62d-6015c758d2ee/original/Safety_ilo.svg">
        </div>
      </div>
    </div>
  </section>
  <section class="fifth-section">
    <div class="row">
      <div class="col-md-4">
        <div class="content">
          <div class="img-box">
            <img src="https://www.uber-assets.com/image/upload/q_auto:eco,c_fill,w_24,h_24/v1542256135/assets/dd/c53d7b-8921-4dc7-93f4-45fb59f4ffb9/original/person-multiple-outlined.svg">
          </div>
          <div class="item">
            <h4 class="content-title">About us</h4>
            <p class="block">Find out how we started, what drives us, and how we’re igniting opportunity.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn more</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
          <div class="img-box">
            <img src="https://www.uber-assets.com/image/upload/q_auto:eco,c_fill,w_24,h_24/v1542254244/assets/eb/68c631-5041-4eeb-9114-80048a326782/original/document-outlined.svg">
          </div>
          <div class="item">
            <h4 class="content-title">Newsroom</h4>
            <p class="block">See announcements about our latest releases, initiatives, and partnerships.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn more</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
          <div class="img-box">
            <img src="https://www.uber-assets.com/image/upload/q_auto:eco,c_fill,w_24,h_24/v1542255370/assets/64/58118a-0ece-4f80-93ee-8041b53593d5/original/home-outlined.svg">
          </div>
          <div class="item">
            <h4 class="content-title">Global citizenship</h4>
            <p class="block">Read about our commitment to making a positive impact in the cities we serve.</p>
            <p class="block2"><a href="#" class="overline-from-left">Learn more</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="six-section">
    <div class="row">
      <div class="col-md-6">
        <a href="#" class="content">
          <div class="signdrive">Sign up to drive<span class="driveicon" style="float: right;"><i class="fas fa-arrow-right"></i></span></div>
        </a>
      </div>
      <div class="col-md-6">
        <a href="#" class="content">
          <div class="signdrive">Sign up to ride<span class="driveicon" style="float: right;"><i class="fas fa-arrow-right"></i></span></div>
        </a>
      </div>
    </div>
  </section>




  
