  <footer>
    <div class="footer-logo"><a href="">Roda</a></div>
    <div class="help-center"><a href="">Visit Help Center</a></div>
    <div class="row">
      <div class="col-md-3">
        <div class="footer-content">
          <h4 class="content-title">Company</h4>
          <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Our offerings</a></li>
            <li><a href="#">Newsroom</a></li>
            <li><a href="#">Investors</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Careers</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="footer-content">
          <h4 class="content-title">Products</h4>
          <ul>
            <li><a href="#">Ride</a></li>
            <li><a href="#">Drive</a></li>
            <li><a href="#">Eat</a></li>
            <li><a href="#">Uber for Business</a></li>
            <li><a href="#">Uber Freight</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="footer-content">
          <h4 class="content-title">Global citizenship</h4>
          <ul>
            <li><a href="#">Safety</a></li>
            <li><a href="#">Diversity and Inclusion</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="footer-content">
          <h4 class="content-title">Innovation</h4>
          <ul>
            <li><a href="#">Advanced Technologies Group</a></li>
            <li><a href="#">AI</a></li>
            <li><a href="#">Elevate</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="follow-social">
          <li><a href="#"><i class="fab fa-facebook-square rd-icon"></i></a></li>
          <li><a href="#"><i class="fab fa-twitter rd-icon"></i></a></li>
          <li><a href="#"><i class="fab fa-youtube rd-icon"></i></a></li>
          <li><a href="#"><i class="fab fa-linkedin rd-icon"></i></a></li>
          <li><a href="#"><i class="fab fa-instagram rd-icon"></i></a></li>
        </div>
      </div>
      <div class="col-md-6">
        <div class="lang">
          <li><a href="#"><i class="fas fa-globe lang-icon"></i>English</a></li>
          <li><a href="#"><i class="fas fa-map-marker-alt lang-icon"></i>Kolkata</a></li>
        </div>
      </div>
      <div class="col-md-3">
        <div class="download">
          <li><a href="#"><img src="https://d1a3f4spazzrp4.cloudfront.net/uber-com/1.3.8/d1a3f4spazzrp4.cloudfront.net/illustrations/app-store-google-4d63c31a3e.svg"></a></li>
          <li><a href="#"><img src="https://d1a3f4spazzrp4.cloudfront.net/uber-com/1.3.8/d1a3f4spazzrp4.cloudfront.net/illustrations/app-store-apple-f1f919205b.svg"></a></li>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="com-name">
          <p>© Roda Business</p>
        </div>
      </div>
      <div class="col-md-6"></div>
      <div class="col-md-3">
        <div class="com-policy">
          <li><a href="<?php echo site_url('privacy-policy') ?>">Privacy</a></li>
          <li><a href="#">Accessibility</a></li>
          <li><a href="#">Terms</a></li>
        </div>
      </div>
    </div>
  </footer>
</body>