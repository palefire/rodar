<div class="container">
	<section class="payment-success">
		<div class="corrrect">
			<i class="fas fa-check-circle"></i>
		</div>
		<h1>Payment Succesful</h1>
		<h2>Thank You For using our Service.</h2>
		<div class="go-back-div">
			<a href="<?php echo site_url() ?>" class="go-back-btn"><i class="fas fa-long-arrow-alt-left"></i>  Go Back To Home.</a>
		</div>
	</section>
</div>