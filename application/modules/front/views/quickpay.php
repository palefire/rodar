<div class="all-main">
		<section class="main-pay">
			<div class="content">
				<div class="top-head">
					<h6>Provide a Customer Details</h6>
				</div>
				<div class="top-body">
					<form method="GET" action="<?php echo site_url('payment') ?>">
						<div class="body-part">
							<label>Enter Your Phone Number<em>*</em></label>
							<input type="text" name="phone_number" required >
						</div>
						<div class="body-part">
							<label>Captcha</label>
							<input type="text" name="" id="captcha" readonly>
							<span id="captcha-refresh">Refresh</span>
						</div>
						<div class="body-part">
							<label>Enter Your Captcha Text<em>*</em></label>
							<input type="text" name="" id="input-captcha">
						</div>
						<div class="body-part">
							<button type="submit" class="submit-btn" id="submit-btn" disabled>Proceed</button>
						</div>
					</form>
				</div>
			</div>
		</section>

		<section class="top-details">
			<div class="content">
				<div class="details-head">
					<h6>Online Pay Quick Tips</h6>
				</div>
				<div class="topic">
					<ul>
						<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
						<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							<ul>
								<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
								<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
								<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>


							</ul>
						</li>
						<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
						<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
					</ul>

				</div>
				<div class="details-footer">
					<h6>Pay Online Within Due Date Lorem Ipsum</h6>
				</div>
			</div>
		</section>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
		 // var random_string = 'sarasij';
		var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	 	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 
 	    	'0','1','2','3','4','5','6','7','8','9');
	    var i;
	    for (i=0;i<6;i++){
			var a = alpha[Math.floor(Math.random() * alpha.length)];
			var b = alpha[Math.floor(Math.random() * alpha.length)];
			var c = alpha[Math.floor(Math.random() * alpha.length)];
			var d = alpha[Math.floor(Math.random() * alpha.length)];
			var e = alpha[Math.floor(Math.random() * alpha.length)];
			var f = alpha[Math.floor(Math.random() * alpha.length)];
			var g = alpha[Math.floor(Math.random() * alpha.length)];
	    }
	    var code = a + b + c + d +  e + f + g;
	    $('#captcha').val(code);
	    $("#captcha-refresh").on('click', function(){
	    	var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		 	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 
	 	    	'0','1','2','3','4','5','6','7','8','9');
		    var i;
		    for (i=0;i<6;i++){
				var a = alpha[Math.floor(Math.random() * alpha.length)];
				var b = alpha[Math.floor(Math.random() * alpha.length)];
				var c = alpha[Math.floor(Math.random() * alpha.length)];
				var d = alpha[Math.floor(Math.random() * alpha.length)];
				var e = alpha[Math.floor(Math.random() * alpha.length)];
				var f = alpha[Math.floor(Math.random() * alpha.length)];
				var g = alpha[Math.floor(Math.random() * alpha.length)];
		    }
		    var code = a + b + c + d +  e + f + g;
		    $('#captcha').val('');
		    $("#submit-btn").attr('disabled', true);
		    $('#captcha').val(code);
	    });

		
		
		$("#input-captcha").on('keyup', function(){
			var input_string = $("#input-captcha").val();
			var output_string = $("#captcha").val();
			// alert(input_string);
			// alert(output_string);
			if( output_string == input_string ){
				// alert('match')
				$("#submit-btn").attr('disabled', false);
			}else{
				$("#submit-btn").attr('disabled', true);
			}
		});
		

	});
</script>