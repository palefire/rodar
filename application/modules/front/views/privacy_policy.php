<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 style="font-size: 36px; font-weight: bold; margin-top: 20px">Privacy Policy.</h2>
			<p style="margin-top: 30px; text-align: justify;">
				Rodar is a commercial app for transportation system. The service is provided by the Rodar management.
			</p>
			<p style="text-align: justify;">
				Unless otherwise stated the terms “you” and “your” refer to the user of the Rodar Services and Rodar operates one website i.e., https://www.rodar.in and two mobile applications i.e., “Rodar” and “Rodar Driver”, we refer all these three collectively as “Rodar Platforms”.
			</p>
			<p style="text-align: justify;">
				This Privacy Policy describes how we collect, process, use, share and protect information about you. The policy is applicable when you access Rodar Platforms through website or mobile or for the data gathered through email, phone or in person when you contact us.
			<p style="text-align: justify;">
				Please read this Policy before using the Rodar Platforms or submitting any personal information to Rodar. BY ACCEPTING THE CUSTOMER TERMS AND CONDITIONS, YOU AGREE TO THE TERMS OF THIS PRIVACY POLICY. If you do not agree with the Policy, please do not use or access the Rodar Platforms. This Privacy Policy is integrated by reference into the Customer Terms and Conditions.
			</p>
			<p style="text-align: justify;">
				If you have questions about this Privacy Policy, please contact us through email address provided on our website and/ or Mobile Application.
			</p>
			<p style="text-align: justify;">
				This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.
			</p>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>A. </b>DEFINITIONS.</h3>
			<p style="text-align: justify;">
				Unless otherwise stated in this Privacy Policy, the terms capitalized in the Privacy Policy shall have the meaning as provided hereunder:
			</p>
			<ul>
				<li>“Device” shall mean computer, mobile or other device used to access the Services.</li>
				<li>“Device Identifier” shall mean IP address or any other unique identifier for the Device.</li>
				<li>“Mobile Application” shall mean an application provided by us on the Device to access the Services.</li>
				<li>“Co-branded Services” shall have the meaning assigned to the term in paragraph 4(c) hereto.</li>
				<li>“Mobile Device Information” shall have the meaning assigned to the term in paragraph 2(d)(ii) hereto.</li>
				<li>“Promotion” shall mean any contest and other promotions offered by us.</li>
				<li>“Protected Information” shall mean such categories of information that could reasonably be used to identify you personally, including your name, e-mail address, and mobile number.</li>
				<li>“Third Party” shall mean a third-party service provider.</li>
				<li>“Rider” and “Driver” will mean the user of our mobile applications called “Rodar” and “Rodar Driver” respectively.</li>
				<li>“Usage Information” shall have the meaning assigned to the term in paragraph 2(b)(i) hereto.</li>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>B. </b>INFORMATION WE COLLECT.</h3>
			<h3 style="font-size: 24px; font-weight: bold; margin-top: 10px"><b>1. </b>INFORMATION YOU PROVIDE TO US: </h3>
			<p style="text-align: justify;">
				We may ask you to provide us with certain Protected Information. We may collect this information through various means and in various places through the Services, or contact us forms, or when you otherwise interact with us. We shall ask you to provide only such Protected Information which is for lawful purpose connected with our Services and necessary to be collected by us for such purpose.
			</p>
			<p style="text-align: justify;">
				Information that you provide comprises the following when you:
			</p>
			<ul>
				<li>Sign up for a Rodar account as a Rider or Driver which may include your Email ID, Name, Address, Mobile Number, Gender, Date of birth, login name, password.</li>
				<li>Sign up for using our Services as a vendor or Rodar Driver, we collect background check and identity verification information which includes location details, vehicle number, all kinds of government authorized documents of the vehicle (if applicable), copies of government identification documents (i.e., Scan copy of ID Proof and Address Proof) and other details (KYC), call and SMS details.</li>
				<li>Update your Rodar account.</li>
				<li>Use Our Services, we may collect and store information about you to process your requests and automatically complete forms for future transactions, including your phone number, address, email, billing information etc.</li>
				<li>Grant access to Rodar of other applications on Your Device for the purpose of using our services or some extra features.</li>
				<li>Report problems for troubleshooting.</li>
				<li>Provide feedback, ratings or compliments as a user.</li>
			</ul>
			<h3 style="font-size: 24px; font-weight: bold; margin-top: 10px"><b>2. </b>INFORMATION WE COLLECT AS YOU ACCESS AND USE OUR SERVICES: </h3>
			<ul>
				<li>In addition to any Protected Information or other information that you choose to submit to us, we and our third-party service provider may use a variety of technologies that automatically (or passively) collect certain information whenever you visit or interact with the Services (“Usage Information”). This Usage Information may include the browser that you are using, the URL that referred you to our Services, all of the areas within our Services that you visit, and the time of day, among other information. In addition, we collect your Device Identifier for your Device. A Device Identifier is a number that is automatically assigned to your Device used to access the Services, and our computers identify your Device by its Device Identifier.</li>
				<li>We also collect some of user specific information to provide you best of our services. This includes all kinds of booking information, feedback, reports etc.</li>
				<li>In addition, tracking information is collected as you navigate through our Services, including, but not limited to geographic areas. The driver’s mobile phone will send your GPS coordinates, during the ride, to our servers. Most GPS enabled mobile devices can define one’s location to within 50 feet.</li>
				<li>Usage Information may be collected using a cookie. If you do not want information to be collected through the use of cookies, your browser allows you to accept or deny the use of cookies. You can control or disable the cookies by setting a preference within your web browser or on your Device. If you choose to disable cookies or flash cookies on your Device, some features of the Services may not function properly or may not be able to customize the delivery of information to you. The Company cannot control the use of cookies (or the resulting information) by third parties, and use of third-party cookies is not covered by our Privacy Policy.</li>
				<li>From time to time, we may supplement the information we collect about you through our website or Mobile Application or Services with outside records from third parties.</li>
			</ul>
			<h3 style="font-size: 24px; font-weight: bold; margin-top: 10px"><b>3. </b>INFORMATION COLLECTED BY MOBILE APPLICATIONS: </h3>
			<ul>
				<li>Our Services are primarily being provided through the Mobile Application. We may collect and use technical data and related information, including but not limited to, technical information about your device, system and application software, and peripherals, that is gathered periodically to facilitate the provision of software updates, product support and other services to you (if any) related to such Mobile Applications.</li>
				<li>When you use any of our Mobile Applications, the Mobile Application may automatically collect and store some or all of the following information from your mobile device (“Mobile Device Information”), in addition to the Device Information, including without limitation:</li>
				<ol>
					<li>Your preferred language and country site (if applicable).</li>
					<li>The manufacturer and model of your mobile device.</li>
					<li>Your mobile operating system.</li>
					<li>The type of mobile internet browsers you are using.</li>
					<li>Your geolocation.</li>
				</ol>
				<li>Information about how you interact with the Mobile Application and any of our web sites to which the Mobile Application links, such as how many times you use a specific part of the Mobile Application over a given time period, the amount of time you spend using the Mobile Application, how often you use the Mobile Application, actions you take in the Mobile Application and how you engage with the Mobile Application.</li>
				<ol>
					<li>Information to allow us to personalize the services and content available through the Mobile Application.</li>
					<li>Data from SMS/ text messages upon receiving Device permissions for the purposes of (i) issuing and receiving one-time passwords and other device verification, and (ii) automatically filling verification details during financial transactions, either through us or a third-party service provider, in accordance with applicable law. We do not share or transfer SMS/ text message data to any third party other than as provided under this Privacy Policy.</li>
					<li>If you are a Rodar driver or vendor, we may record your calls with us or related call details, SMS details location and address details to improve our services and for future reference.</li>
				</ol>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>C. </b>USE OF INFORMATION COLLECTED.</h3>
			<ul>
				<li>Rodar uses the collected data to provide you the best experience of our services. We use your information to closely monitor which features of the services are being used most, to allow you to view your trip history, rate trips and thus help us to personalize, maintain, and improve our services.</li>
				<li>We use geographic locations to determine where we should offer or focus services, features and/or resources.</li>
				<li>We provide some of your Protected Information (such as your name, pick up address, contact number) to the Rodar driver who accepts your request for transportation so that the driver may contact and find you.</li>
				<li>We use that geo-location information for various purposes, including for you to be able to view the available Rodar drivers in your area that are close to your location, for you to set your pick-up location, for the drivers to identify the pick-up location, to send you promotions and offers, and to allow you (if you choose through any features we may provide) to share this information with other people.</li>
				<li>If you are registering your account with an email, we will send you a welcoming email to verify your username and password and for mobile numbers we will send OTP to confirm the same.</li>
				<li>We use your Internet Protocol (IP) address to help diagnose problems with our computer server, and to administer our web site(s). Your IP address is used to help identify you, but contains no personal information about you.</li>
				<li>The information collected from our Mobile Application are being used to serve you the correct app version depending on your device type, for troubleshooting and in some cases, marketing purposes.</li>
				<li>We will send you strictly service-related announcements on rare occasions when it is necessary to do so. For instance, if our Services are temporarily suspended for maintenance or suspended only for you for any other reasons like payment not made etc., we might send you an email or SMS. If you do not wish to receive them, you have the option to deactivate your account.</li>
				<li>We will send SMS or email to the drivers to notify the payment in every specified interval.</li>
				<li>We use the data to verify drivers’ identity, background history, and eligibility to use our services.</li>
				<li>In addition, we may use your Protected Information or Usage Information that we collect about you:
					<ol>
						<li>To provide you with information or services or process transactions that you have requested or agreed to receive including to send you electronic newsletters, or to provide you with special offers or promotional materials on behalf of us or third parties.</li>
						<li>To enable you to participate in a variety of the Services’ features such as contests or other promotions;</li>
						<li>For testing, research, product enhancement & development, data analytics and machine learning to improve the user experience. </li>
						<li>To contact you regarding the changes of our services or any service disruption or changes in our policies.</li>
						<li>For any legal purposes, to trace any claims, disputes or complains.</li>
					</ol>
				</li>
				<li>We may use your data marketing and targeted advertising. But we do not sell users’ personal data to, or share it with, such partners or others for purposes of their own direct marketing or advertising, except with users’ consent.</li>
				<li>We can suspend our services for any particular drivers if the driver fails to make our payment even after sending multiple notifications by email or SMS and also can resume once payment is made.</li>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>D. </b>INFORMATION SHARING AND DISCLOSURE.</h3>
			<p style="text-align: justify;">
				We do not share, sell, rent or trade the data whatever we use to collect about you other than as disclosed within this Privacy Policy or at the you provide your information. Your information may be shared in different situations or conditions with different users, partners and/or service providers of Rodar as mentioned below.
			</p>
			<ul>
				<li>With other users-<br>We share certain information between Rider and Driver to enable communication to start a trip or to other users related to any claims or complains. It includes the followings:</li>
				<li>
					<ol>
						<li>Riders’ name, contact details, rating, current location while booking a vehicle and pick-up and/or drop-off locations with drivers.</li>
						<li>Drivers’ name, photo, contact details, rating, current location, vehicle colour, govt provided number (if having), number provided by us (i.e., Rodar) with riders.</li>
						<li>If users participate in our referral program that might occur for a certain period of time, we may share certain personal data between the referred user and the user who referred.</li>
					</ol>
				</li>
				<li>With third parties providing services on behalf of Rodar-<br>
					We use third party companies and individuals to assist and smoothen our services. Your protected information will be disclosed to such third parties and the shared information will be subject to the privacy policy and practices of such third parties. We are not responsible for the privacy policies and practices of such third parties and therefore, you should review the privacy policies and practices of such third parties prior to agreeing the terms of this Privacy Policy of Rodar.<br>We share the data to our service providers, marketing partners, vendors, business partners and agents. This includes the following.</li>
					<li>
						<ol>
							<li>Payment processors and facilitators.</li>
							<li>Background check and identity verification providers and/or may be individuals hired by us.</li>
							<li>Google, as we use Google Cloud and Google Maps for Rodar Platforms.</li>
							<li>Third party company use to develop and support our Rodar platforms on behalf of us (Now it is The Palefire PVT. LTD.)</li>
							<li>Marketing partners and marketing platform providers, including social media advertising services.</li>
							<li>Research partners or individuals, including those performing surveys or research projects in partnership with Rodar or on behalf of Rodar.</li>
							<li>Rodar may conduct promotions and contests in association with any third parties. If you participate in those contests and promotions, your Protected Information may be disclosed to such third parties or the public in connection with the administration of such Promotion, including, in connection with winner selection, prize fulfilment, and as required by law or permitted by the Promotion’s official rules, such as on a winners list.</li>
							<li>Third parties associated with us to conduct market study and data analysis to improve, customize and enhance our services.</li>
						</ol>
					</li>
					<li>With Administrative for legal reasons-<br>Rodar will share your personal and protected information to the Government (State and/or Central) and law enforcement officials and other third parties to satisfy any applicable law, regulation, subpoenas, Governmental requests or for any legal process. It is due to the reasons which includes as follows.</li>
					<li>
						<ol>
							<li>To protect Rodar’s property and rights.</li>
							<li>To protect other’s property and rights if hampered by any of the Rodar user and/or any user having a connection with the use of Rodar Platforms.</li>
							<li>To enforce the safety of Rodar users and/or public for any reasons.</li>
							<li>To enforce our Terms of services and user agreements.</li>
							<li>To detect and/or prevent any fraud and security breach issues.</li>
							<li>To investigate a dispute or claim relating to the use of Rodar Platforms.</li>
							<li>To stop and/or prevent activity which we may consider to be or having a risk of being an illegal, unethical, or legally actionable activity.</li>
						</ol>
					</li>
					<li>Business transfer-<br>Rodar might share your personal information with others during the sale of company assets or merger or acquisition of all or a portion of our business by or into another company.</li>
					<li>Third parties linked to Rodar Platforms for advertisements or any other reasons-<br>Rodar platforms may have contents and/or links and/or any advertisements supplied by third parties and when you clicked on those, you might be directed to a third-party website and/or webpage that we do not control. Your website usage information and device identifier may be collected by such third parties and we are not responsible for the privacy policy or practices followed by these third parties. It also includes the social media features and widgets that might be linked or might run on our online website and mobile services. </li>
					<li>With others at user’s request and consent-<br>Rodar may share certain user’s information like ETA, location or trip data with any particular person when it is mentioned and requested by that user only. We might share a user’s personal and protected data with any other third parties or individuals if we notify that user with the same and he/she consent to the sharing.</li>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>E. </b>DATA COLLECTED BY RODAR USERS.</h3>
			<p style="text-align: justify;">
				As described above, we will exchange certain personal information between driver and rider to start a trip, during a trip or in a trip history. The usage of this information (collected by any person may be driver or rider) is not covered in this Privacy Policy. 
			</p>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>F. </b>PERSONAL DATA MAINTENANCE AND MODIFICATION.</h3>
			<ul>
				<li>You should always provide the correct data of yourself and your vehicle (for Rodar driver) during account registration. You are also responsible in maintaining the accuracy of your personal and protected information provided to us. If any of your personal information (like contact number, vehicle number) changes, you should modify your Rodar account with the same updated information. If you no longer wish to access our services, you can cancel or delete your account.</li>
				<li>If we find any of your personal information provided by you is wrong or misleading or missing, we may not permit you to access our services due to insufficiency or inaccuracy of the information.</li>
				<li>You can also modify your communication preferences or default options like Payment, Billing etc by logging into your user’s account.</li>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>G. </b>DATA RETENTION AND DELETION</h3>
			<p style="text-align: justify;">
				Rodar will retain the user’s protected information and usage data as long as it is needed to fulfil the below purposes.</p>
			<ul>
				<li>For any legal or regulatory requirements.</li>
				<li>To settle an outstanding credit.</li>
				<li>To resolve any outstanding dispute or claims.</li>
				<li>For fraud prevention</li>
				<li>For the purpose of safety and security</li>
				<li>For data analytics and any other business reasons.</li>
			</ul>
			<p style="text-align: justify;">Users may delete their account at any time but even after deletion, information of those users will be retained for the purposes as mentioned above.</p>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>H. </b>SECURITY</h3>
			<p style="text-align: justify;">
				The User’s personal data and Usage Information we collect is securely stored within our databases, and we use standard, industry-wide, reasonable security practices such as encryption,firewalls and SSL (Secure Socket Layers) to protect your information. However, no security system is impenetrable. We cannot guarantee the security of our databases and also cannot guarantee that information you supply won't be intercepted while being transmitted to us or the associated third parties over the Internet or wireless communication. So, any information you transmit to the Company you do at your own risk. We always recommend you not to disclose your user id and password to anyone.</p>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>H. </b>TERMS OF USE</h3>
			<ul>
				<li>Rider should always provide the exact number of passengers for whom the booking of a trip is made. It is always expected to give the details prior to book a vehicle but rider can change or update during the trip.</li>
				<li>Driver should always confirm or approve the number of passengers mentioned by Rider. For any discrepancy, trip cannot be started.</li>
				<li>Rider has to pay the fare determined by our application to the Driver at the end of a trip by cash or by any digital payments whatever is chosen by him/her.</li>
				<li>Driver will only take or accept the exact amount from Rider whatever is shown by our application at the end of a trip.</li>
				<li>Driver is liable to pay an amount decided by Rodar for using our application. We will generate a bill for each of the driver according to their trips at every certain period of interval and driver has to make the payment using by the application or our online website. If any driver fails to make the payment, we can suspend the service for that driver at any point of time.</li>
				<li>We ensure proper back ground check and identity verification of the driver but cannot guarantee the safety and security of the Rider during the trip, it is always Rider’s own risk. But for any illegal activities or offences or mishap during the trip while accessing our service, we will always cooperate with the users along with the Government or law enforcement officials at our best.</li>
			</ul>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>I. </b>GRIEVANCE CELL</h3>
			<p style="text-align: justify;">
				The grievance officer would be appointed by Rodar for the purposes of the rules drafted under the Information Technology Act, 2000, who may be contacted at grievances@rodar.in You may address your grievances or complains in respect of this privacy policy or usage of your Protected Information or for any other reasons to him/her.</p>
			<h3 style="font-size: 28px; font-weight: bold; margin-top: 10px"><b>K. </b>UPDATES TO THE PRIVACY POLICY AND PRACTICES</h3>
			<p style="text-align: justify;">
				Even so often we may update this policy to mitigate the changes in our privacy practices and/or terms of use. If we make any updates, we will notify the user through our Rodar Platforms or through any other means like email, SMS etc. The revised Privacy Policy will be effective immediately after posting it. We always suggest to periodically visit our privacy page through our online website or Rodar Platforms to remain updated with the latest information on our Privacy Policy and Practices.</p>
		</div>
	</div>
</div>