<!DOCTYPE html>
<html>
<head>
  <title>
    <?php

      // if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'front' ){
      //   echo 'Wokk || Home';
      // }
      


    ?>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta charset="UTF-8">
  <!-- <link rel="icon" href="<?php echo $this->data['img_path'] ?>logo.png" type="" sizes="16x16"> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&family=PT+Sans:wght@400;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
  <link rel="stylesheet" href="<?php echo $this->data['css_path'] ?>styles.css?ver=<?php echo microtime();  ?>">
  <link rel="stylesheet" href="<?php echo $this->data['css_path'] ?>responsive.css?ver=<?php echo microtime()  ?>">
  
</head>
<body>
  <header>
    <div class="left-side">
      <div class="img-logo">
        <a href="<?php echo site_url('') ?>"><img src="<?php echo site_url('/assets/images/logo2.png ') ?>"></a>
      </div>
      <div class="logo"><a href="<?php echo site_url('') ?>">Rodar</a></div>
      <div class="hd-company">
       <div class="dropdown">
          <button class="dropdown-toggle" type="button" data-toggle="dropdown">Company
          <span class="glyphicon glyphicon-chevron-down"></span></button>
          <ul class="dropdown-menu">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Quick Pay</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="right-side">
      <!-- <div class="login"><a href="#"><i class="fas fa-user-alt" style="margin-right:10px;font-size:14px"></i> Login</div></a> -->
      <div class="signup"><a href="<?php echo site_url('quick-pay') ?>">Quick Pay</a></div>
    </div>
  </header>
 