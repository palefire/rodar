<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        // $this->load->model('admin/adminmodel');
        $this->load->model('frontmodel');
        // if( isset($this->session->userdata['wokk_user_id']) ){
        // 	$user_details = $this->frontmodel->get_user_by_id( $this->session->userdata['wokk_user_id'] );
        // 	$this->data['view_count'] = $this->frontmodel->get_view_nos( $this->session->userdata['wokk_user_id'] );
        // }else{
        // 	$user_details = NULL;
        // }

        // $this->data['user_detail'] = $user_details;
	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		//$all_services = $this->adminmodel->get_all_services_without_general();
		$this->load->view('header', array(
			//user_details' => $this->data['user_detail'],
		));	
		$this->load->view('home', array(
		// 	'all_services'	=>$all_services,
		));	
		$this->load->view('footer');	
	}//index function

	/*
	|--------------------------------------------------------------------------
	| quickpay function
	|--------------------------------------------------------------------------
	*/

	public function quickpay(){
		//$all_services = $this->adminmodel->get_all_services_without_general();
		$this->load->view('header', array(
			//user_details' => $this->data['user_detail'],
		));	
		$this->load->view('quickpay', array(
		// 	'all_services'	=>$all_services,
		));	
		$this->load->view('footer');	
	}//index function

		/*
	|--------------------------------------------------------------------------
	| Payment function
	|--------------------------------------------------------------------------
	*/

	public function payment(  ){
		$phone_number = $this->input->get('phone_number');
		$driver_details = $this->frontmodel->fetch_driver_details( $phone_number );
		// print_r($driver_details);
		
		$this->load->view('header', array(
			//user_details' => $this->data['user_detail'],
		));	
		$this->load->view('payment', array(
			'driver_details'	=>$driver_details,
		));	
		$this->load->view('footer');
			
			
	}//index function

	public function payment_success(){
		$tran = $this->input->post();
		// print_r($tran);
		$transaction_id = $tran['razorpay_payment_id'];
		$phone_number = $tran['phone_number'];
		$driver_details = $this->frontmodel->send_transaction_details( $phone_number, $transaction_id );
		$this->load->view('header', array(
			//user_details' => $this->data['user_detail'],
		));	
		$this->load->view('payment_success', array(
			// 'driver_details'	=>$driver_details,
		));	
		$this->load->view('footer');
	}//fn

	public function privacy_policy(){
		$this->load->view('header', array(
			//user_details' => $this->data['user_detail'],
		));
		$this->load->view('privacy_policy', array(
			// 'driver_details'	=>$driver_details,
		));
		$this->load->view('footer');
	}//fn

	
}