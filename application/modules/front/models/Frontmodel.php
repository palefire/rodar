<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Frontmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }


    /**
     * fetch driver details FUNCTION
     */
   
   public function fetch_driver_details( $phone_number ){
        // password_hash($password, CRYPT_BLOWFISH),
        $data = array(
            'apiCredential' =>array(
                                'apiLogin'  => password_hash('6PYhsrfF', CRYPT_BLOWFISH),
                                'apiPass'   => password_hash('fKXxdMt6', CRYPT_BLOWFISH)
                              ),
            'mobile_number'=> $phone_number
        );
        $data_string = json_encode($data);
        $url = 'http://amrachestakorchi.in/api/driver_billing_data/';
        // $url = 'http://192.168.0.17/roda/api/driver_billing_data/';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        //'Content-Type: multipart/form-data',
        'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        // Free up the resources $curl is using
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        }else{
            return json_decode($response, true);
        }
   }//fn

   public function send_transaction_details( $phone_number, $transaction_id ){
        // password_hash($password, CRYPT_BLOWFISH),
        $data = array(
            'apiCredential' =>array(
                                'apiLogin'  => password_hash('6PYhsrfF', CRYPT_BLOWFISH),
                                'apiPass'   => password_hash('fKXxdMt6', CRYPT_BLOWFISH)
                              ),
            'mobile_number' => $phone_number,
            'transaction_id'=> $transaction_id
        );
        $data_string = json_encode($data);
        $url = 'http://amrachestakorchi.in/api/save_billing_informations/';
        // $url = 'http://192.168.0.17/roda/api/save_billing_informations/';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        //'Content-Type: multipart/form-data',
        'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        // Free up the resources $curl is using
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        }else{
            return json_decode($response, true);
        }
   }//fn
}//class