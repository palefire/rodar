<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        if( !isset($this->session->userdata['wokk_admin_id']) ){
			return redirect('admin/login');
			
		}
        
         $this->load->model('adminmodel');
         $admin_details = $this->adminmodel->get_admin_by_id( $this->session->userdata['wokk_admin_id'] );
         $this->data['admin_name'] = $admin_details['admin_name'];
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		// echo $cookie = get_cookie('wokk_set_me');
		// echo $this->session->userdata['wokk_admin_id'];
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('dashboard',array(
			'total_registered' => $this->adminmodel->get_no_of_all_user(),
			'total_users' => $this->adminmodel->get_no_of_simple_user(),
			'total_card_holders' => $this->adminmodel->get_no_of_card_holders(),
		));
		$this->load->view('footer');
		
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Service function
	|--------------------------------------------------------------------------
	*/

	public function services(){
		$service_details =  $this->adminmodel->get_all_services_with_pagination();
		$all_services = $service_details['all_services']; 
		 
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('service', array(
			'all_services' => $all_services,
			'pagination'=> $service_details['pagination']
		));
		$this->load->view('footer');
		
	}//fn

	public function add_service(){
		$service_name = $this->input->post('service_name');
		$no_of_existing_service =  $this->adminmodel->get_no_of_services( $service_name );
		$slug = $this->adminmodel->create_slug( $service_name );
		if( $no_of_existing_service == 0 ){
			$service_slug = $slug;
		}else{
			$service_slug = $slug.'-'.$no_of_existing_service;
		}

		$config['upload_path']          = './uploads/services/images/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 4500;
        $config['max_height']           = 3000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('service_image')){
        	$upload_error = $this->upload->display_errors();
            $this->session->set_flashdata( 'upload_error' , $upload_error );
			return redirect('admin/services');
        }else{
        	$data1 =  $this->upload->data();
            $img = $data1["file_name"];

	        if ( ! $this->upload->do_upload('icon_img')){
	        	$upload_error = $this->upload->display_errors();
	            $this->session->set_flashdata( 'upload_error1' , $upload_error );
				return redirect('admin/services');
	        }else{
	        	$data2 =  $this->upload->data();
            	$icon = $data2["file_name"];
	        	$data = array(
					'service_name'  => $service_name,
					'service_slug' 	=> $service_slug,
					'service_icon'	=> $icon,
					'service_image'	=> $img,
					'service_created_by'	=> $this->session->userdata['wokk_admin_id'],
				);


				$return = $this->db->insert('wokk_services', $data);
				if( $return ){
					return redirect('admin/services');
				}else{
					$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
					return redirect('admin/services');
				}
	        }
		}
	}//fn

	public function edit_service( $service_id ){
		$service_details = $this->adminmodel->get_service_by_id( $service_id );
		// print_r( $service_details );
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('edit_service', array(
				'service_details'=>$service_details,
				
			)
		);
		$this->load->view('footer');
	}//function

	public function edit_service_name_db(){
		$service_name = $this->input->post('service_name');
		$service_id = $this->input->post('service_id');
		$data = array(
				'service_name' => $service_name,
			);
		$return = $this->db->update('wokk_services', $data, array('service_id' => $service_id));
		if( $return ){
        	$this->session->set_flashdata( 'success' , 'Successfully Saved!' );
        	return redirect("admin/edit_service/$service_id");
        }else{
        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
        	return redirect("admin/edit_service/$service_id");
        }
		// $img = $_FILES['service_image']['name'];
		// $icon = $_FILES['icon_img']['name'];
	}//fn

	public function edit_service_icon_db(){
		$service_id = $this->input->post('service_id');
		$icon = $_FILES['icon_img']['name'];
		if( empty( $icon ) ){
			$this->session->set_flashdata( 'image_not_set' , 'Image not Selected' );
        	return redirect("admin/edit_service/$service_id");
		}else{
			$config['upload_path']          = './uploads/services/images/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';
	        $config['max_size']             = 2000;
	        $config['max_width']            = 4500;
	        $config['max_height']           = 3000;
	        $this->load->library('upload', $config);
	         if ( ! $this->upload->do_upload('icon_img')){
	        	$upload_error = $this->upload->display_errors();
	            $this->session->set_flashdata( 'upload_error' , $upload_error );
				return redirect("admin/edit_service/$service_id");
	        }else{
	        	$data2 =  $this->upload->data();
            	$icon = $data2["file_name"];
            	$data = array(
					'service_icon' => $icon,
				);

				$return = $this->db->update('wokk_services', $data, array('service_id' => $service_id));
				if( $return ){
		        	$this->session->set_flashdata( 'success' , 'Successfully Saved!' );
		        	return redirect("admin/edit_service/$service_id");
		        }else{
		        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
		        	return redirect("admin/edit_service/$service_id");
		        }
	        }

		}
		
		
	}//fn
	public function edit_service_image_db(){
		$service_id = $this->input->post('service_id');
		$icon = $_FILES['service_image']['name'];
		if( empty( $icon ) ){
			$this->session->set_flashdata( 'image_not_set' , 'Image not Selected' );
        	return redirect("admin/edit_service/$service_id");
		}else{
			$config['upload_path']          = './uploads/services/images/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';
	        $config['max_size']             = 2000;
	        $config['max_width']            = 4500;
	        $config['max_height']           = 3000;
	        $this->load->library('upload', $config);
	         if ( ! $this->upload->do_upload('service_image')){
	        	$upload_error = $this->upload->display_errors();
	            $this->session->set_flashdata( 'upload_error' , $upload_error );
				return redirect("admin/edit_service/$service_id");
	        }else{
	        	$data2 =  $this->upload->data();
            	$icon = $data2["file_name"];
            	$data = array(
					'service_image' => $icon,
				);

				$return = $this->db->update('wokk_services', $data, array('service_id' => $service_id));
				if( $return ){
		        	$this->session->set_flashdata( 'success' , 'Successfully Saved!' );
		        	return redirect("admin/edit_service/$service_id");
		        }else{
		        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
		        	return redirect("admin/edit_service/$service_id");
		        }
	        }

		}
		
		
	}//fn

	public function delete_service(){
		$service_id = $this->input->post('service_id');
		$delete = $this->db->delete('wokk_services', array('service_id' => $service_id));
		if($delete){
			echo $service_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| layouts function
	|--------------------------------------------------------------------------
	*/

	public function layouts(){
		$q0 = $this->db->get('wokk_layout'); 
        $no_of_layouts = $q0->num_rows();
        $config['base_url'] = site_url('/admin/layouts');
        $config['total_rows'] = $no_of_layouts;
        $config['per_page'] = 5;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->order_by('layout_id','ASC')
                        ->join('wokk_services','wokk_layout.layout_service_id = wokk_services.service_id' )
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_layout');
        $layout_details = $query->result_array();
        $pagination = $this->pagination->create_links();
		// $layout_details =  $this->adminmodel->get_all_layout_with_pagination()['all_layouts'];
		// $pagination =$this->adminmodel->get_all_layout_with_pagination();
		// print_r($pagination);
		$all_services =  $this->adminmodel->get_all_services();
		 // print_r($layout_details);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('layout', array(
			'all_layouts' => $layout_details,
			'pagination'=> $pagination,
			'all_services' => $all_services,
		));
		$this->load->view('footer');
	}//fn

	public function add_layout(){
		$layout_name = $this->input->post('layout_name');
		$layout_service = $this->input->post('layout_service');

		$config['upload_path']          = './uploads/layouts/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 4500;
        $config['max_height']           = 3000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('layout_image')){
        	$upload_error = $this->upload->display_errors();
            $this->session->set_flashdata( 'upload_error' , $upload_error );
			return redirect('admin/services');
        }else{
        	$data1 =  $this->upload->data();
            $img = $data1["file_name"];
            $data = array(
            	'layout_name'	 	=> $layout_name,
            	'layout_service_id'	=> $layout_service,
            	'layout_image'	 	=> $img,
            	'created_by'	    =>$this->session->userdata['wokk_admin_id']
            );

            $return = $this->db->insert('wokk_layout', $data);
            if( $return ){
				return redirect('admin/layouts');
			}else{
				$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/layouts');
			}
        }
	}//fn

	public function edit_layout( $layout_id ){
		$all_services =  $this->adminmodel->get_all_services();
		$layout_details =  $this->adminmodel->get_layout_by_id( $layout_id );
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('edit_layout', array(
			'all_services' => $all_services,
			'layout_details' => $layout_details,
		));
		$this->load->view('footer');
	}//fn

	public function edit_layout_db(){
		$layout_id = $this->input->post('layout_id');
		$layout_name = $this->input->post('layout_name');
		$layout_service = $this->input->post('layout_service');

		$img = $_FILES['layout_image']['name'];
		if( empty( $img ) ){
			$data = array(
            	'layout_name'	 	=> $layout_name,
            	'layout_service_id'	=> $layout_service,
            	'modified_by'	    =>$this->session->userdata['wokk_admin_id']
            );

			$return = $this->db->update('wokk_layout', $data, array('layout_id' => $layout_id));
	        if( $return ){
	        	return redirect("admin/edit_layout/$layout_id");
	        }else{
	        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
	        	return redirect("admin/edit_layout/$layout_id");
	        }
		}else{

			$config['upload_path']          = './uploads/layouts/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';
	        $config['max_size']             = 2000;
	        $config['max_width']            = 4500;
	        $config['max_height']           = 3000;
	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('layout_image')){
	        	$upload_error = $this->upload->display_errors();
	            $this->session->set_flashdata( 'upload_error' , $upload_error );
				return redirect('admin/services');
	        }else{
	        	$data1 =  $this->upload->data();
	            $img = $data1["file_name"];
	            $data = array(
	            	'layout_name'	 	=> $layout_name,
	            	'layout_service_id'	=> $layout_service,
	            	'layout_image'	 	=> $img,
	            	'modified_by'	    =>$this->session->userdata['wokk_admin_id']
	            );

	           	$return = $this->db->update('wokk_layout', $data, array('layout_id' => $layout_id));
	            if( $return ){
					return redirect('admin/layouts');
				}else{
					$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
					return redirect('admin/layouts');
				}	
			}		

		}
	}//fn

	public function delete_layout(){
		$layout_id = $this->input->post('layout_id');
		$delete = $this->db->delete('wokk_layout', array('layout_id' => $layout_id));
		if($delete){
			echo $layout_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//fn
	// ------------------------------Users--------------------------------

	public function all_users(){

		$query_result = $this->adminmodel->get_all_users();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('user_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn
	public function live_users(){
		$query_result = $this->adminmodel->live_users();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('user_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn
	public function blocked_users(){
		$query_result = $this->adminmodel->blocked_users();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('user_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn

	public function all_card_holders(){
		$query_result = $this->adminmodel->get_card_holders();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('card_holder_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn
	public function live_card_holders(){
		$query_result = $this->adminmodel->live_card_holders();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('card_holder_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn
	public function blocked_card_holders(){
		$query_result = $this->adminmodel->blocked_card_holders();
		$all_users = $query_result['all_users'];
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('card_holder_list', array(
			'all_users' => $all_users,
			'pagination'=> $query_result['pagination']
		));
		$this->load->view('footer');
	}//fn


	public function block_user(){
		$user_id = $this->input->post('user_id');
		$update = $this->db->update('wokk_user', array('user_status' => '5'),array('user_id' => $user_id));
		if($update){
			echo 1;
		}else{
			echo 0;
		}
	}//fn
	public function unblock_user(){
		$user_id = $this->input->post('user_id');
		$update = $this->db->update('wokk_user', array('user_status' => '1'),array('user_id' => $user_id));
		if($update){
			echo 1;
		}else{
			echo 0;
		}
	}//fn

	public function search_user(){
		$phone_number = $this->input->get('phone_number');
		$all_users = $this->adminmodel->get_user_by_phone($phone_number);
		// print_r($all_users);
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('search_user_list', array(
			'all_users' => $all_users,
		));
		$this->load->view('footer');
	}//fn

	public function extend_user_card_validity(){
		$user_id 		= $this->input->post('user_id');
		$phone_number 	= $this->input->post('phone_number');
		$validity_time 	= $this->input->post('validity_time');
		$user_details 	= $this->adminmodel->get_user_by_id( $user_id );
		// print_r($user_details);
		$extend_validity = $this->adminmodel->extend_card_validity( $user_id, $validity_time );
		if( $extend_validity ){
			return redirect( site_url('admin/search_user?phone_number=').$phone_number );
		}else{
			return redirect( site_url('admin/search_user?phone_number=').$phone_number );
		}

	}//fn

	// ------------------------------OTP--------------------------------

	public function otp(){
		$q = $this->db->get('wokk_otp');
		$all_otp = $q->result_array();

		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('otp_list', array(
			'all_otp'	=> $all_otp,
		));
		$this->load->view('footer');
	}//fn
// ------------------------------Settings--------------------------------
	public function admin_settings(){
		$new_card_validity = $this->get_meta('new_card_validity');
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('admin_settings', array(
			'new_card_validity'	=> $new_card_validity,
		));
		$this->load->view('footer');
	}//fn

	public function save_admin_settings_to_db(){
		$new_card_validity = $this->input->post('new_card_validity');
		if($this->update_meta('new_card_validity', $new_card_validity)){
			$this->session->set_flashdata( 'settings_saved' , 'Saved' );
			return redirect('admin/admin_settings');
		}
	}//fn


	public function website_settings(){
		$company_email = $this->get_meta('company_email');
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('website_settings', array(
			'company_email'	=> $company_email,
		));
		$this->load->view('footer');
	}//fn

	public function save_website_settings_to_db(){
		$company_email = $this->input->post('company_email');
		if($this->update_meta('company_email', $company_email)){
			$this->session->set_flashdata( 'settings_saved' , 'Saved' );
			return redirect('admin/website_settings');
		}
	}//fn

	public function app_settings(){
		$new_card_validity = $this->get_meta('new_card_validity');
		$this->load->view('header', array(
			'admin_name'	 => $this->data['admin_name'],
		));
		$this->load->view('website_settings', array(
			'new_card_validity'	=> $new_card_validity,
		));
		$this->load->view('footer');
	}//fn

}//class