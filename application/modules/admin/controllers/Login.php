<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

       
        $this->load->model('adminmodel');
        
	}

	/*
	|--------------------------------------------------------------------------
	| Login function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['wokk_admin_id']) ){
			return redirect('admin/');
		}
		
		$remember_token = get_cookie('wokk_set_me');
		$check_remember_me = $this->adminmodel->check_remember_me( $remember_token);

		if( $check_remember_me ){
			//log in the user
			print_r($check_remember_me);
			$this->session->set_userdata( 'wokk_admin_id', $check_remember_me['admin_id'] );
			$this->session->set_userdata( 'wokk_admin_email', $check_remember_me['admin_email'] );
			return redirect('admin');
		}else{
			$this->load->view('login');
		}

		
		
	}//fn

	public function verify_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$kpln = $this->input->post('kpln');

		$login_success = $this->adminmodel->checklogin( $username, $password);
		if( $login_success ){
			$this->session->set_userdata( 'wokk_admin_id', $login_success['admin_id'] );
			$this->session->set_userdata( 'wokk_admin_email', $login_success['admin_email'] );
			
			//set Remember Me Functionality	
			if( $kpln ){
				$admin_id = $login_success['admin_id'];
				$admin_email = $login_success['admin_email'];
				$admin_username = $login_success['admin_username'];
				$token = md5($admin_email.$admin_username);
				$remember_me_token = $token; 
				$data = array(
					'remember_me_token' => $remember_me_token,
				);
				$token_update = $this->db->update('wokk_admin', $data, array('admin_id' => $admin_id));
				if( $token_update ){
					//create cookies
					
				    set_cookie('wokk_set_me', $remember_me_token, time()+6500);
				}
			}

			return redirect('admin');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('admin/login');
		}
	}//fn

	public function logout(){
		$email = $this->session->userdata['wokk_admin_email'];
		$this->session->unset_userdata('wokk_admin_id');
		$this->session->unset_userdata('wokk_admin_email');
		$data = array(
					'remember_me_token' => NULL,
				);
		$token_update = $this->db->update('wokk_admin', $data, array('admin_email' => $email));
	    delete_cookie('wokk_set_me');

		
		
		return redirect('admin/login' );
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function dashboard(){

		$this->load->view('header');
		$this->load->view('dashboard');
		$this->load->view('footer');
		
	}//fn
}