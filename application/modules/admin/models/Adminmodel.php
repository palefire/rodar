<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Adminmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }


    /**
     * Checklogin FUNCTION
     */
    public function checklogin( $username, $password){
        $q = $this->db
                    ->where( array('admin_username'=>$username) )
                    ->get('wokk_admin');
        $result = $q->result_array();
        if( $q->num_rows() > 0 ){
            $password_db = $result[0]['admin_password'];
            if(password_verify( $password, $password_db)) {
                return $result[0];
            }else{
                return FALSE;
            }

        }else{
            return FALSE; 
        }
    }//fn

     /**
     * Check Remember Me FUNCTION
     */
    public function check_remember_me( $token){
        if( $token != '' ){
            $q = $this->db
                    ->where( array('remember_me_token'=>$token) )
                    ->get('wokk_admin');
            $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }//fn


    /**
     * Services FUNCTION
     */
    public function get_no_of_services( $service_name = NULL ){
        if( $service_name == NULL ){
            $q = $this->db->get('wokk_services');
            return $q->num_rows();
        }else{
           $q = $this->db
                    ->where( array('service_name'=>$service_name) )
                    ->get('wokk_services'); 
            return $q->num_rows();
        }
        
    }//fn

    public function get_all_services(){
        $q = $this->db->get('wokk_services'); 
            return $q->result_array();
    }
    public function get_all_services_without_general(){
        $q = $this->db->where("`service_id` <> '1'")->get('wokk_services'); 
            return $q->result_array();
    }

    public function get_all_services_with_pagination(){
        $q0 = $this->db->get('wokk_services'); 
        $no_of_service = $q0->num_rows();
        $config['base_url'] = site_url('/admin/services');
        $config['total_rows'] = $no_of_service;
        $config['per_page'] = 5;

        $this->pagination->initialize($config);
        $query =  $this->db->order_by('service_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('wokk_services');
        $service_details = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_services' => $service_details,
        );
    }

    public function get_service_by_id( $service_id ){
        $q = $this->db->where( array( 'service_id' => $service_id))->get('wokk_services'); 
            return $q->result_array()[0];
    }


    /**
     * Create Slug FUNCTION
     */
    public function create_slug( $name ){
        $slug = strtolower($name); 
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        return $slug;
    }

    /**
     * Create Layout FUNCTION
     */

    public function get_all_layout_with_pagination(){
        $q0 = $this->db->get('wokk_layout'); 
        $no_of_layouts = $q0->num_rows();
        $config['base_url'] = site_url('/admin/layouts');
        $config['total_rows'] = $no_of_layouts;
        $config['per_page'] = 5;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->order_by('layout_id','ASC')
                        ->join('wokk_services','wokk_layout.layout_service_id = wokk_services.service_id' )
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_layout');
        $layout_details = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_layouts' => $layout_details,
        );
    }//fn

    public function get_layout_by_id( $layout_id ){
        $q = $this->db->where( array( 'layout_id' => $layout_id))->get('wokk_layout'); 
            return $q->result_array()[0];
    }//fn

    public function get_admin_by_id( $admin_id ){
        $q = $this->db->where( array( 'admin_id' => $admin_id))->get('wokk_admin'); 
        return $q->result_array()[0];
    }

   
    public function get_all_users(){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/all_users');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '0'))
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn
    public function live_users(){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/live_users');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '0', 'user_status'=>'1'))
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn
    public function blocked_users(){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/blocked_users');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '0', 'user_status'=>'5'))
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn

    public function get_card_holders( ){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/all_card_holders');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->select('wokk_user.*,wokk_services.service_name')
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '1'))
                        ->join('wokk_services', 'wokk_user.user_service_id = wokk_services.service_id')
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn
    public function live_card_holders(){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/live_users');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->select('wokk_user.*,wokk_services.service_name')
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '1', 'user_status'=>'1'))
                        ->join('wokk_services', 'wokk_user.user_service_id = wokk_services.service_id')
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn
    public function blocked_card_holders(){
        $q0 = $this->db->get('wokk_user'); 
        $no_of_users = $q0->num_rows();
        $config['base_url'] = site_url('/admin/blocked_users');
        $config['total_rows'] = $no_of_users;
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $query =  $this->db
                        ->select('wokk_user.*,wokk_services.service_name')
                        ->order_by('user_id','DESC')
                        ->where(array('user_role' => '1', 'user_status'=>'5'))
                        ->join('wokk_services', 'wokk_user.user_service_id = wokk_services.service_id')
                        ->limit($config['per_page'],$this->uri->segment(3))
                        ->get('wokk_user');
        $all_users = $query->result_array();
        $pagination = $this->pagination->create_links();

        return array(
            'pagination' => $pagination,
            'all_users' => $all_users,
        );
    }//fn

    public function get_user_by_phone( $user_phone ){
        $query =  $this->db
                        ->select('wokk_user.*,wokk_services.service_name')                     
                        ->where(array('user_phone' => $user_phone))
                        ->join('wokk_services', 'wokk_user.user_service_id = wokk_services.service_id')                      
                        ->get('wokk_user');
        return $query->result_array();
    }//fn

    public function get_user_by_id( $user_id ){
        $query =  $this->db
                        ->select('wokk_user.*,wokk_services.service_name')                     
                        ->where(array('user_id' => $user_id))
                        ->join('wokk_services', 'wokk_user.user_service_id = wokk_services.service_id')                      
                        ->get('wokk_user');
        return $query->result_array()[0];
    }//fn

    public function get_no_of_all_user($status=null){
        if( $status == null ){
            $query =  $this->db
                        ->get('wokk_user');
        }else{
            $query =  $this->db
                        ->where(array('user_status'=> $status))
                        ->get('wokk_user');
        }
        return $query->num_rows();
    }//fn
    public function get_no_of_simple_user($status=null){
        if( $status == null ){
            $query =  $this->db
                        ->where(array('user_role'=> '0'))
                        ->get('wokk_user');
        }else{
            $query =  $this->db
                        ->where(array('user_status'=> $status, 'user_role'=> '0'))
                        ->get('wokk_user');
        }
        return $query->num_rows();
    }//fn
    public function get_no_of_card_holders($status=null){
        if( $status == null ){
            $query =  $this->db
                        ->where(array('user_role'=> '1'))
                        ->get('wokk_user');
        }else{
            $query =  $this->db
                        ->where(array('user_status'=> $status, 'user_role'=> '1'))
                        ->get('wokk_user');
        }
        return $query->num_rows();
    }//fn

    public function if_meta_key_exist( $meta_key ){
        $query =  $this->db
                        ->where(array('meta_key'=> $meta_key))
                        ->get('wokk_meta');
        if( $query->num_rows()>0 ){
            return true;
        }else{
            return false;
        }
    }// fn
    public function create_meta( $meta_key ){
        $return = $this->db->insert('wokk_meta', array('meta_key' => $meta_key));
        if( $return ){
            true;
        }else{
            false;
        }
    }//fn

    public function update_meta($meta_key, $meta_value){
        $update_meta = $this->db->update('wokk_meta', array( 'meta_value' => $meta_value ), array('meta_key' => $meta_key));
        if( $update_meta ){
            return true;
        }else{
            return false;
        }
    }//fn

    public function get_meta( $meta_key ){
        $query =  $this->db
                        ->where(array('meta_key'=> $meta_key))
                        ->get('wokk_meta');
        if( $query->num_rows()>0 ){
            return $query->result_array();
        }else{
            return false;
        }
    }//fn


    public function get_users_valid_10_days(){
        $q6 = $this->db
                    ->where(array('user_card_valid_until' => date("Y/m/d", strtotime("+ 10 days"))) )
                    ->get('wokk_user');
        return $q6->result_array();
    }//fn

    public function get_users_valid_3_days(){
        $q6 = $this->db
                    ->where(array('user_card_valid_until' => date("Y/m/d", strtotime("+ 3 days"))) )
                    ->get('wokk_user');
        return $q6->result_array();
    }//fn

    public function get_device_id($user_id){
        $q6 = $this->db
                    ->select('wokk_user.user_device_id')
                    ->where(array('user_id' => $user_id) )
                    ->get('wokk_user');
        return $q6->result_array()['user_device_id'];
    }//fn

    public function check_validity( $user_id ){
        $date = date();
        $date_sql = "`user_card_valid_until` < '".$date."'";

        $q = $this->db
                    ->where(array('user_id'=>$user_id ))
                    ->where($date_sql)
                    ->get('wokk_user');
        if( $q->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }//fn
    public function extend_card_validity( $user_id, $validity_time ){
        $date = date('Y/m/d');
        $date_sql = "`user_card_valid_until` > '".$date."'";

        $q = $this->db
                    ->where(array('user_id'=>$user_id ))
                    ->where($date_sql)
                    ->get('wokk_user');
        if( $q->num_rows() > 0 ){
            $user_card_valid_until = $q->result_array()[0]['user_card_valid_until'];
            if( $validity_time == '6' ){
                $new_validity_date = date('Y-m-d', strtotime("+6 months", strtotime($user_card_valid_until)));
            }elseif( $validity_time == '12' ){
                $new_validity_date = date('Y-m-d', strtotime("+12 months", strtotime($user_card_valid_until)));
            }
            

            $update_validity = $this->db->update('wokk_user', array( 'user_card_valid_until' => $new_validity_date ), array('user_id' => $user_id));
            if($update_validity){
                return $update_validity;
            }else{
                return false;
            }
            return $effectiveDate;
        }else{
            return false;
        }
    }//fn
  
}//class
